<?php

class SchedReportsController extends BaseController {

    public function delete()
    {
		//this function is used to delete a scheduled report
        $json = Input::get('json','[]');
		//this file has some specific configuration to enable/disable user privacy for the scheduled reports
		$schedUserFile = base_path() . "\\storage\\schedule_user.txt";
		$enabSchedUser = 0;
		//if the file exist read from it if not it will create it
		if (file_exists($schedUserFile))
		{
			$enabSchedUser = (int) file_get_contents($schedUserFile);
		} else { //create the file
			$enabSchedUser = 0;
			file_put_contents($schedUserFile, $enabSchedUser , LOCK_EX);
		}
        try
        {
            $jsonArray = json_decode($json, true);
            if(empty($jsonArray))
                return '0';
			//delete the scheduled report
            ScheduleReport::destroy($jsonArray);
            return '1';
        } catch (Exception $e) {
            return '0';
        }
    }

    public function enabled()
    {
		//function to enable/disable a scheduled report
		//the json object will contain the id of the scheduled reports  that the user wnats to enable/disable
        $ids = Input::get('json');
        $ids = json_decode($ids,true);
        foreach($ids as $id=>$en)
        {
            try
            {
				//then it will loop trough the database to enable/disable the one with the same id	
                ScheduleReport::where('Id',$id)->update(array('Enabled' => $en));
            } catch (Exception $e) {
                return '0';
            }
        }
        return '1';
    }

    public function edit()
    {
        /*
            1.) Using the id, get the report row
            2.) Convert this row into a json object to be passed to the client
        */
        $id = Input::get('id',1);
        $sched = ScheduleReport::where('RecCnt',$id)->first();
        return View::make('reports')->with('sched',$sched);

    }

    public function generate()
    {
        $id = Input::get('id');
        $report = new Report();
        $sReport = ScheduleReport::find($id)->toArray();
        $date_beg = "";
        $date_end = "";

        date_default_timezone_set('UTC');
        $todayDate = date('d-M-y');


        foreach($sReport as $k=>$parameter)
        {
            //echo $k.'<br>';
            $report->$k = $parameter;
            switch ($sReport['DATE_BEG'])
            {
                case 0:{$date_beg =  $todayDate; break;}
                case 1:{$date_beg =  $todayDate; break;}
                case 2:{$date_beg =  date('d-M-y', strtotime('-1 day')); break;}
                case 3:{$date_beg =  date('d-M-y', strtotime('-2 day')); break;}
                case 4:{$date_beg =  date('d-M-y', strtotime('-3 day')); break;}
                case 5:{$date_beg =  date('d-M-y', strtotime('-4 day')); break;}
                case 6:{$date_beg =  date('d-M-y', strtotime('-5 day')); break;}
                case 7:{$date_beg =  date('d-M-y', strtotime('-6 day')); break;}
                case 8:{$date_beg =  date('d-M-y', strtotime('-7 day')); break;}
                case 9:{$date_beg =  date('d-M-y', strtotime('-8 day')); break;}
                case 10:{$date_beg =  date('d-M-y', strtotime('-9 day')); break;}
                case 11:{$date_beg =  date('d-M-y', strtotime('-10 day')); break;}
                case 12:{$date_beg =  date('d-M-y', strtotime('-11 day')); break;}
                case 13:{$date_beg =  date('d-M-y', strtotime('-12 day')); break;}
                case 14:{$date_beg =  date('d-M-y', strtotime('-13 day')); break;}
                case 15:{$date_beg =  date('d-M-y', strtotime('-14 day')); break;}
                case 16:{$date_beg =  date('d-M-y', strtotime('-15 day')); break;}
                case 17:{$date_beg =  date('d-M-y', strtotime('-16 day')); break;}
                case 18:{$date_beg =  date('d-M-y', strtotime('-17 day')); break;}
                case 19:{$date_beg =  date('d-M-y', strtotime('-18 day')); break;}
                case 20:{$date_beg =  date('d-M-y', strtotime('-19 day')); break;}
                case 21:{$date_beg =  date('d-M-y', strtotime('-20 day')); break;}
                case 22:{$date_beg =  date('d-M-y', strtotime('-21 day')); break;}
                case 23:{$date_beg =  date('d-M-y', strtotime('-22 day')); break;}
                case 24:{$date_beg =  date('d-M-y', strtotime('-23 day')); break;}
                case 25:{$date_beg =  date('d-M-y', strtotime('-24 day')); break;}
                case 26:{$date_beg =  date('d-M-y', strtotime('-25 day')); break;}
                case 27:{$date_beg =  date('d-M-y', strtotime('-26 day')); break;}
                case 28:{$date_beg =  date('d-M-y', strtotime('-27 day')); break;}
                case 29:{$date_beg =  date('d-M-y', strtotime('-28 day')); break;}
                case 30:{$date_beg =  date('d-M-y', strtotime('-29 day')); break;}
                case 31:{$date_beg =  date('d-M-y', strtotime('-30 day')); break;}
                case 32:{$date_beg =  date('d-M-y', strtotime('-31 day')); break;}
                case 33:{$date_beg =  date('d-M-y', strtotime('-32 day')); break;}
                case 34:{$date_beg =  date('d-M-y', strtotime('first day of this month'));break;}
                case 35:{$date_beg =  date('d-M-y', strtotime('last day of this month')); break;}
                case 36:{$date_beg =  date('d-M-y', strtotime('first day of last month')); break;}
                case 37:{$date_beg =  date('d-M-y', strtotime('last day of last month')); break;}
                case 38:{$date_beg =  date('d-M-y', strtotime('- 1 month')); break;}
           }
           switch ($sReport['DATE_END'])
            {
                case 0:{$date_end =  $todayDate; break;}
                case 1:{$date_end =  $todayDate; break;}
                case 2:{$date_end =  date('d-M-y', strtotime('-1 day')); break;}
                case 3:{$date_end =  date('d-M-y', strtotime('-2 day')); break;}
                case 4:{$date_end =  date('d-M-y', strtotime('-3 day')); break;}
                case 5:{$date_end =  date('d-M-y', strtotime('-4 day')); break;}
                case 6:{$date_end =  date('d-M-y', strtotime('-5 day')); break;}
                case 7:{$date_end =  date('d-M-y', strtotime('-6 day')); break;}
                case 8:{$date_end =  date('d-M-y', strtotime('-7 day')); break;}
                case 9:{$date_end =  date('d-M-y', strtotime('-8 day')); break;}
                case 10:{$date_end =  date('d-M-y', strtotime('-9 day')); break;}
                case 11:{$date_end =  date('d-M-y', strtotime('-10 day')); break;}
                case 12:{$date_end =  date('d-M-y', strtotime('-11 day')); break;}
                case 13:{$date_end =  date('d-M-y', strtotime('-12 day')); break;}
                case 14:{$date_end =  date('d-M-y', strtotime('-13 day')); break;}
                case 15:{$date_end =  date('d-M-y', strtotime('-14 day')); break;}
                case 16:{$date_end =  date('d-M-y', strtotime('-15 day')); break;}
                case 17:{$date_end =  date('d-M-y', strtotime('-16 day')); break;}
                case 18:{$date_end =  date('d-M-y', strtotime('-17 day')); break;}
                case 19:{$date_end =  date('d-M-y', strtotime('-18 day')); break;}
                case 20:{$date_end =  date('d-M-y', strtotime('-19 day')); break;}
                case 21:{$date_end =  date('d-M-y', strtotime('-20 day')); break;}
                case 22:{$date_end =  date('d-M-y', strtotime('-21 day')); break;}
                case 23:{$date_end =  date('d-M-y', strtotime('-22 day')); break;}
                case 24:{$date_end =  date('d-M-y', strtotime('-23 day')); break;}
                case 25:{$date_end =  date('d-M-y', strtotime('-24 day')); break;}
                case 26:{$date_end =  date('d-M-y', strtotime('-25 day')); break;}
                case 27:{$date_end =  date('d-M-y', strtotime('-26 day')); break;}
                case 28:{$date_end =  date('d-M-y', strtotime('-27 day')); break;}
                case 29:{$date_end =  date('d-M-y', strtotime('-28 day')); break;}
                case 30:{$date_end =  date('d-M-y', strtotime('-29 day')); break;}
                case 31:{$date_end =  date('d-M-y', strtotime('-30 day')); break;}
                case 32:{$date_end =  date('d-M-y', strtotime('-31 day')); break;}
                case 33:{$date_end =  date('d-M-y', strtotime('-32 day')); break;}
                case 34:{$date_end =  date('d-M-y', strtotime('first day of this month')); break;}
                case 35:{$date_end =  date('d-M-y', strtotime('last day of this month')); break;}
                case 36:{$date_end =  date('d-M-y', strtotime('first day of last month')); break;}
                case 37:{$date_end =  date('d-M-y', strtotime('last day of last month')); break;}
                case 38:{$date_end =  date('d-M-y', strtotime('- 1 month')); break;}
           }
        }
        $report->Stage = 0;
        //Calculate these values based on schedule report parameter
        $report->DATE_BEG = $date_beg;//"08-May-14";
        $report->DATE_END = $date_end;

        unset($report['Id'], $report['EditDate'], $report['SchedMem'],
              $report['Enabled'],$report['SchedTime'],
              $report['SchedDate'], $report['IntervalType'],
              $report['MonthDays'], $report['WeekDays'],
              $report['IntervalDays'], $report['RepeatMinutesYN'],
              $report['RepeatMinutes'], $report['RepeatDaysYN'],
              $report['RepeatDays'], $report['ExpireTime']);
 
        //generate timstamp/insert report
        $controller = new ReportController();
        $report['CMB_FORMAT'] = '0'; // html
        $timestamp = $controller->insertReport($report);
 
        return View::make('/report_generating')->with('timestamp',$timestamp);
    }

    public function update()
    {
        //this function is used to update any param of a scheduled report 
		$report = json_decode(Input::get('json'), true);
        $sched = ScheduleReport::find($report['Id']);
		
        unset($report['WebFilename'], $report['Stage']);

        foreach($report as $key=>$attr)
        {
            if (isset($attr))
            {
                $sched->$key = $attr;
            }
        }
	
        try
        {
            $sched->save();
			$schedUserFile = base_path() . "\\storage\\schedule_user.txt";
			$enabSchedUser = 0;
			if (file_exists($schedUserFile))
			{
				$enabSchedUser = (int) file_get_contents($schedUserFile);
			} else { //create the file
				$enabSchedUser = 0;
				file_put_contents($schedUserFile, $enabSchedUser , LOCK_EX);
			}
			if($enabSchedUser == 0)
			{
				$this->scheduledReportUser($report['RecCnt']);
			}
            return '1';

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
	
	public function scheduledReportUser($schedRptId)
    {
        //this is used to give privacy to the users to only be able to see and edit the scheduled reports created by themselves 
		$webUser = Auth::user();
		$table = "tableName"	;
        $query = "SELECT * FROM information_schema.tables WHERE table_schema = 'dbName' AND table_name = 'tableName'";
        $tableExists = DB::select(DB::raw($query)); //checking if table doesn't exist
        $table = "tableName";
        if($tableExists == null)
        {
            ScheduleUsers::CreateTable($table);
            $schedUser = new ScheduleUsers;
            $schedUser->SchedId = $schedRptId;
            $schedUser->UserId = $webUser->WebUserKey;
            $schedUser->save();
        }
        else {
            $schedUser = new ScheduleUsers;
            $schedUser->SchedId = $schedRptId;
            $schedUser->UserId = $webUser->WebUserKey;
            $schedUser->save();//restirct permissions by user
        }
    }
	
	public function DeleteSchedUser($schedRptId)
	{
		//remove association from the database between the scheduled report and the user that created it
		$table = "tableName"	;
        $query = "SELECT * FROM information_schema.tables WHERE table_schema = 'dbName' AND table_name = 'tableName'";
        $tableExists = DB::select(DB::raw($query)); //checking if table doesn't exist
        $table = "tableName";
        if($tableExists != null)
        {
            $schedUser = ScheduleUsers::where("SchedId", $schedRptId)->delete();
        }
	}
}

