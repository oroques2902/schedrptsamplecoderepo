<?php

//This is a test to dump a laravel db config object into the server
// for database.php to grab.
// This way we can control db config automagically at install without physically
//  traversing the directory by self populating this structure

$dbconf = array(

		'mysql' => array(
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'dbName',
			'username'  => 'username',
			'password'  => 'password',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
            'port'      => 3306,
		),
	);

return $dbconf;
