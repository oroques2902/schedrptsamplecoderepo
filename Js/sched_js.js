$body = $("body");

$(document).on({
    ajaxStart: function () {
        $body.addClass("loading");
    },
    ajaxStop: function () {
        $body.removeClass("loading");
    }
});
function Uncheck() {
    var tablLen = $("#user_datagrid_tab0 tr").length - 1;
    var i;
    for (i = 0; i < tablLen; i++)

    {
        var name = "select" + (i + 1);
        if ($("#" + name).length != 0) {
            if ($("#" + name).is(':checked')) {
                $("#" + name).attr('checked', false);
            }
        } else {
            return;
        }
    }
}
$(function () {
    $("#dialog-form").dialog({
        autoOpen: false,
        height: 500,
        width: 450,
        modal: true,
        buttons: {
            "Ok": function () {

            },
            Cancel: function () {
                $(this).dialog("close");
            }
        },
        close: function () {

        }
    });
    $("#turnOnOff")
        .button()
        .click(function () {
            TurnOnOff();
        });
    $("#edit")
        .button()
        .click(function () {
 Edit();
        });
    $("#delete")
        .button()
        .click(function () {
            Delete();
        });
    $("#test")
        .button()
        .click(function () {
            Generate();
        });
    $("#deleteM")
        .button()
        .click(function () {
            Delete();
        });
    $("#loadM")
        .button()
        .click(function () {
          Edit();
        });
});

$(function () {
    if ($('#sched_type').val() == "scheduled") {
        $('#btnsDiv').show();
        $('#btnsLoadDiv').hide();
    } else {
        $('#btnsLoadDiv').show();
        $('#btnsDiv').hide();
    }

});

function Delete() {
    var obj = new Object();
    obj.User = [];
    var tablLen = $("#user_datagrid_tab0 tr").length - 1;
    for (var i = 0; i < tablLen; i++) {
        var pos = i + 1;
        var name = "select" + pos;
        var chkbox;
        if (document.getElementsByName(name).length != 0) {
            chkbox = document.getElementById(name);
            var rows = $('#user_datagrid_tab0 tr');
            var values = rows[pos].cells;
            if (chkbox.checked) {
                obj.User[i] = values[0].innerHTML;
            }
        }
    }
    var str_jsonS = (JSON.stringify(obj.User));
    var type = $('#sched_type').val();
    var urlInit = "/reports/" + type + "/delete" ;
    $.ajax({
    url : urlInit,
    data: {json: str_jsonS},
    type: "GET",
    success: function(data, textStatus, jqXHR)
    {
        document.location.reload();
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
    	alert(errorThrown);
    }
    });
}

function SelectRow() {
    $("#user_datagrid_tab0 tr").click(function () {
        var selected = $(this).hasClass("red-cell");
        $("#user_datagrid_tab0 tr").removeClass("red-cell");
        if (!selected)
            $(this).addClass("red-cell");
    });
}

function TurnOnOff() {

    var obj = new Object();
    var numberId;
    var tablLen = $("#user_datagrid_tab0 tr").length - 1;
    for (var i = 0; i < tablLen; i++) {
        var pos = i + 1;
        var name = "select" + pos;
        var chkbox;
        if (document.getElementsByName(name).length != 0) {
            chkbox = document.getElementById(name);
            var rows = $('#user_datagrid_tab0 tr');
            var values = rows[pos].cells;
            if (chkbox.checked) {
                var active ;
                if(values[4].innerHTML == "Y")
                {
                  active = "N";
                }
                else if(values[4].innerHTML == "N")
                {
                  active = "Y";
                }

                numberId = values[0].innerHTML;
                obj[numberId] = active;
            }
        }
    }

    var str_jsonS = (JSON.stringify( obj));
    var type = $('#sched_type').val();
    var urlInit = "/reports/" + type + "/enabled";
    $.ajax({
    url : urlInit,
    data: {json: str_jsonS},
    type: "GET",
    success: function(data, textStatus, jqXHR)
    {
       if (data == "1")
       {
            Uncheck();
            document.location.reload();

        } else
        {
            alert(data);
        }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
    alert(errorThrown);
    }
    });

}

function Edit() {
    var obj = new Object();
    var RecCnt = new Array();
    var tablLen = $("#user_datagrid_tab0 tr").length - 1;
    for (var i = 0; i < tablLen; i++) {
        var pos = i + 1;
        var name = "select" + pos;
        var chkbox;
        if (document.getElementsByName(name).length != 0) {
            chkbox = document.getElementById(name);
            var rows = $('#user_datagrid_tab0 tr');
            var values = rows[pos].cells;
            if (chkbox.checked) {
                RecCnt.push(values[0].innerHTML);
            }
        }
    }
    var request = new XMLHttpRequest();
    var type = $('#sched_type').val();
    for (var i = 0; i < RecCnt.length; i++)
    {
        var url = "/reports/" + type + "/edit?id=" + RecCnt[i];
        window.open(url);
    }
}
function Generate() {
    var obj = new Object();
    var RecCnt = new Array();
    var tablLen = $("#user_datagrid_tab0 tr").length - 1;
    for (var i = 0; i < tablLen; i++) {
        var pos = i + 1;
        var name = "select" + pos;
        var chkbox;
        if (document.getElementsByName(name).length != 0) {
            chkbox = document.getElementById(name);
            var rows = $('#user_datagrid_tab0 tr');
            var values = rows[pos].cells;
            if (chkbox.checked) {
                RecCnt.push(values[0].innerHTML);
            }
        }
    }
    var request = new XMLHttpRequest();
    var type = $('#sched_type').val();
    for (var i = 0; i < RecCnt.length; i++)
    {
        var url = "/reports/" + type + "/generate/?id=" + RecCnt[i];
        window.open(url);
    }
}

