@extends('main_layout')

@section('main_menu')
<script src="/js/sched_js.js"></script>
    <ul id="jMenu" style="width:1px; margin-top:-10px">
        <li >
        </li>
    </ul>
@stop

@section('main_page_title')
{{ $type == 'scheduled' ? 'Scheduled Reports' : 'Memorized Reports' }}
@stop

@section('main_back_button')
  <a href="/"><img src="/img/back-icon1.png" onmouseover=" this.src='/img/back-iconAct.png'" onmouseout=" this.src='/img/back-icon1.png'" alt="" width="40" height="40"></a>
@stop
@section('main_content')

<div id="tabs" style="height:810px">
    @yield('sched_hidden')
    <div id="btnsDiv" style="margin-left:10px; margin-top:10px">
        <table>
        <tr>
            <td><input name="turnOnOff" id="turnOnOff" type="button" value="Turn Off/On" ></td>
            <td><input name="edit" id="edit" type="button" value="Edit" ></td>
            <td><input name="delete" id="delete" type="button" value="Delete" ></td>
            <td><input name="test" id="test" type="button" value="Test/Run Now" ></td>
        </tr>
        </table>
    </div>
    <div id="btnsLoadDiv" style="margin-left:10px; margin-top:10px; display:none">
        <table>
        <tr>
            <td><input name="deleteM" id="deleteM" type="button" value="Delete" ></td>
            <td><input name="loadM" id="loadM" type="button" value="Load" ></td>
        </tr>
        </table>
    </div>
    <div id ="tableDiv" class="schedTable">
        <table id="user_datagrid_tab0">
            @yield('sched_table')
        </table>
    </div>
</div>
<div class="modal"><!-- Place at bottom of page --></div>
@stop
