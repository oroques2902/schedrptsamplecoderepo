@extends('scheduled_reports_layout')
<?php
/*
    This page shows all the currently scheduled reports.
    Only admin users are allowed to edit, delete, or modify the reports

*/

//$type sent from route
$t = $type == 'scheduled' ? 'S' : 'M';
$schedUserFile = base_path() . "\\storage\\schedule_user.txt";
$enabSchedUser = 0;
if (file_exists($schedUserFile))
{
	$enabSchedUser = (int) file_get_contents($schedUserFile);
} else { //create the file
	$enabSchedUser = 0;
	file_put_contents($schedUserFile, $enabSchedUser , LOCK_EX);
}
if($enabSchedUser == 1)
{
	$webUser = Auth::user();
	$webUserId = $webUser->WebUserKey;
	$query = "SELECT * FROM information_schema.tables WHERE table_schema = 'dbName' AND table_name = 'tableName'";
	$tableExists = DB::select(DB::raw($query));
	$reports = array();
	$table = "tableName"	;

	if($tableExists == null)
	{
		ScheduleUsers::CreateTable($table);
	}
	else
	{
	   $schedRptId = ScheduleUsers::where('UserId', $webUserId)->lists('SchedId');
		if($schedRptId != null)
	   $reports = ScheduleReport::where('SchedMem',$t)->whereIn('Id', $schedRptId)->get();
	}
}
else
{
	$reports = ScheduleReport::where('SchedMem',$t)->get();
}
//show the reports
?>
//creating the table that contains a list of the scheduled reports so the user can add/delete/edit a scheduled report
@section('sched_table')
<thead>
    <th id="ldapThTd" style="display:none"></th>
    <th id="ldapThTd">Select</th>
    <th id="ldapThTd">Report Description</th>
    <th id="ldapThTd">Run time</th>
    <th id="ldapThTd">Active</th>
    <th id="ldapThTd">Last Edited</th>
</thead>
<?php $i = 1 ?>
@foreach($reports as $report)
<tr id="ldapThTd">
    <td id="ldapThTd" style="display:none">{{$report->Id}}</td>
    <td id="ldapThTd" style="width:2%">
        <form id="selectCbx" action="">
            <input type="checkbox" name="{{ 'select'.$i }}" id="{{ 'select'.$i }}" >
        </form>
    </td>
    <td id="ldapThTd" style="width:40%;">{{$report->ReportDesc}}</td>
    <td id="ldapThTd">{{$report->SchedTime}}</td>
    <td id="ldapThTd">{{$report->Enabled}}</td>
    <td id="ldapThTd">{{$report->EditDate}}</td>
</tr>
<?php $i++ ?>
@endforeach
<?php
while($i < 30)
{
    echo '<tr id=ldapThTd><td id=ldapThTd></td><td id=ldapThTd style="width:2%"></td><td id=ldapThTd></td><td id=ldapThTd></td><td id=ldapThTd></td></tr>';
    $i++;
}
?>
@stop

@section('sched_hidden')
{{ Form::hidden('sched_type', $type, array('id'=>'sched_type')) }}
@stop
