<?php

    //requires reports permissions
    Route::group (array('before' => 'auth.reports'), function()
    {
  
        /*------------- Scheduled & Memorized Reports -----------------*/

        //type is scheduled or memorized
        Route::group(array('before' => 'reports.type'), function()
        {
            Route::any('reports/{type}', function($type)
            {
                return View::make('scheduled_reports', array('type'=>$type));
            });
            Route::get('reports/{type}/delete', array('before'=>'contains_json',
                                                      'uses'=>'SchedReportsController@delete'));
            Route::get('reports/{type}/enabled', array('before'=>'contains_json',
                                                       'uses' => 'SchedReportsController@enabled'));
            Route::get('reports/{type}/edit', 'SchedReportsController@edit');

            //uses generate report from reports
            Route::get('reports/{type}/generate',
                        array('before' => 'contains_id',
                              'uses' => 'SchedReportsController@generate'));

            Route::get('reports/{type}/update',
                       array('before' => 'contains_json',
                             'uses' => 'SchedReportsController@update'));
        });

    });

    
});
?>