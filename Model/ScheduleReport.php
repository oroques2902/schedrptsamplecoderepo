<?php

class ScheduleReport extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tableName';

    //to avoid weirdness
    public $timestamps = false;

    //cause
   // public $incrementing = false;

    public $primaryKey = 'Id';

	/**
	 * Get the unique identifier for the div.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

}
